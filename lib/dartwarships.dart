library warships;

import 'dart:html';
import 'dart:math';

part 'src/model.dart';
part 'src/controller.dart';
part 'src/view.dart';

const ROWCOUNT = 16;
const COLCOUNT = 8;

